package archetypes.app.demo

interface DemoService {
  fun greetings(name: String): String
}

class DefaultDemoService : DemoService {
  override fun greetings(name: String): String {
    return "hello $name from service"
  }
}

package archetypes.app.config

import archetypes.app.demo.DefaultDemoService
import archetypes.app.demo.DemoService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class Services {

  @Bean
  open fun demoService(): DemoService {
    return DefaultDemoService()
  }
}

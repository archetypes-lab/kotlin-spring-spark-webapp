package archetypes.app.config

import archetypes.app.demo.DemoService
import archetypes.app.http.spark.handlers.BaseSparkHandler
import archetypes.app.http.spark.handlers.DemoHandler
import io.github.cdimascio.dotenv.Dotenv
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import spark.TemplateEngine
import spark.kotlin.Http
import spark.kotlin.ignite
import spark.template.freemarker.FreeMarkerEngine

@Configuration
open class Base {

  private val log = LoggerFactory.getLogger(javaClass)

  @Bean
  open fun foo(dotenv: Dotenv): String {
    return dotenv["FOO"]
  }

  @Bean
  open fun dotenv(): Dotenv {
    return Dotenv.configure().ignoreIfMissing().ignoreIfMalformed().load()
  }

  // http

  @Bean
  open fun httpSpark(): Http {
    val http = ignite()
    http.port(8080)
    return http
  }

  @Bean
  open fun sparkTemplateEngine(): TemplateEngine {
    return FreeMarkerEngine()
  }

  @Bean
  open fun sparkDemoHandler(
      http: Http,
      templateEngine: TemplateEngine,
      demoService: DemoService
  ): BaseSparkHandler {
    return DemoHandler(http, templateEngine, demoService)
  }

  @Bean
  open fun corsOriginPattern(dotenv: Dotenv): String {
    return dotenv["CORS_ORIGIN_PATTERN"]
  }
}

package archetypes.app.http.spark.handlers

import archetypes.app.demo.DemoService
import spark.ModelAndView
import spark.TemplateEngine
import spark.kotlin.Http
import spark.kotlin.RouteHandler

class DemoHandler(http: Http, templateEngine: TemplateEngine, val demoService: DemoService) :
    BaseSparkHandler(http, templateEngine) {

  init {
    http.get("/api/v1/hello/:name", "text/plain", ::greeting)
    http.get("/demo", "text/html", templeteEngine, ::demoView)
  }

  private fun demoView(rh: RouteHandler): ModelAndView {
    return ModelAndView(emptyMap<String, Any>(), "demo.html")
  }

  private fun greeting(rh: RouteHandler): String {
    val name = rh.params(":name")
    return demoService.greetings(name)
  }
}

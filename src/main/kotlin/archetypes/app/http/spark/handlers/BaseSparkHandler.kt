package archetypes.app.http.spark.handlers

import spark.TemplateEngine
import spark.kotlin.Http

abstract class BaseSparkHandler(val http: Http, val templeteEngine: TemplateEngine) {}

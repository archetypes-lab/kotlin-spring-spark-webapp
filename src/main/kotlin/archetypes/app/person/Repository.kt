package archetypes.app.person

interface PersonRepository {
    fun GetAll(): List<Person>
    fun FindByName(name: String): List<Person>
}

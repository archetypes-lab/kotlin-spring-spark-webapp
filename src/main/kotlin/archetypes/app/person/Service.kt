package archetypes.app.person

interface PersonService {
    fun GetAll(): List<Person>
    fun FindByName(name: String): List<Person>
}

class PersonServiceImpl(private val repository: PersonRepository) : PersonService {

    override fun GetAll(): List<Person> {
        return this.repository.GetAll()
    }

    override fun FindByName(name: String): List<Person> {
        return this.repository.FindByName(name)
    }
}

package archetypes.app.person

data class Person(val id: Long?, val name: String, val age: Int)
